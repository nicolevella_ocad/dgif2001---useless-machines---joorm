/*
  Project:      Useless Machines - JOORM
  Student:      Nicole Vella, Arashjot Kaur, Yuqian Sui, and Cam Saunders
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   October 27, 2019
  Based on:     IRremote Library; Rafi Khan; https://www.arduinolibraries.info/libraries/i-rremote
                Stepper Library; Arduino Libraries; https://www.arduinolibraries.info/libraries/stepper
                Dummies guide on driving a 28BYJ-48 Stepper Motor; Helen; https://www.seeedstudio.com/blog/2019/03/04/driving-a-28byj-48-stepper-motor-with-a-uln2003-driver-board-and-arduino/
*/



// libraries
#include <IRremote.h> // include the IR library so we can detect codes sent from an infared remote/signal
#include <Servo.h>    // include the servo library so we can control the servo motor
#include <Stepper.h>  // include the stepper library so we can control the stepper motor



// variables
const int numSteps    = 2038;   // number of steps in our stepper motor
const int servo       = 13;     // servo on pin 6
const int receiver    = 11;     // IR receiver sensor on pin 11
unsigned long codeReceived;       // stored signal from remote, we use an unsigned long variable because the code will be a long non-negative/positive integer



// init motors & receiver
Stepper armMotion(numSteps, 2, 3, 4, 5);  // init a Stepper motor called armMotion on pins 2,3,4,5
Servo handMotion;                         // init a Servo called handMotion
IRrecv irrecv(receiver);                  // init an IR receiver
decode_results results;                   // decode_results is a method in the IR library that allows us to decode the signal from the reemote



void setup() {
  irrecv.enableIRIn();      // enable the receiver to begin accepting signals/codes
  armMotion.setSpeed(6);    // set the speed of the arm/stepper
  handMotion.attach(servo); // attach the servo motor instance to it's pin on the arduino
}



void loop() {

  if (irrecv.decode(&results)) {    // Check to see if we've received a code from the reemote

    if (results.value != 4294967295) {  // this is the HOLD code, if the code received is not the HOLD code, a new button has been pressed
      codeReceived = results.value;       // store the code of the new button pressed
    } 
    
    switch (codeReceived) { // We use a switch statement to check the value of codeReceived because an IF statment caused unexpected results

      case 551510175:   // this is the code for thee RIGHT ARROW on the LG remote we're using, when it's pressed, this section of the Switch will trigger
        moveArmRight(); // a function to move the arm right using a stepper motor
        break;          // break out of the switch

      case 551542815:  // this is the code for thee LEFT ARROW
        moveArmLeft(); // a function to move the arm right using a stepper motor
        break;

      case 551486205:  // this is the code for thee UP ARROW
        moveHandUp();  // a function to move the hand up using a servo motor
        break;

      case 551518845:   // this is the code for thee DOWN ARROW
        moveHandDown(); // a function to move the hand down using a servo motor
        break;

    }

    irrecv.resume();    // Resume receiving signals/get the next code from the remote

  }

}



void moveArmRight() {   // func to move the arm right
  armMotion.step(+10);  // move the stepper/arm 10 steps clockwise
}


void moveArmLeft() {    // func to move the arm left
  armMotion.step(-10);  // move the stepper/arm 10 steps counter-clockwise
}


void moveHandUp() {     // func to move the hand up
  handMotion.write(90); // set the angle on the servo/hand to 90 degrees (up position)
}


void moveHandDown() {   // func to move the hand down
  handMotion.write(0);  // set the angle on the servo/hand to 0 degrees (down position)
}


